#include <windows.h>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
#include <algorithm>


using namespace std;

bool debugMode = false;
bool showComMap = false;

const std::string startRed("\033[0;31m");
const std::string startYellow("\033[0;33m");
const std::string endColor("\033[0m");

struct Coordinate{
	int row;
	int col;
};

/*-----------------------------------------------------------------*
 * Ship Class													   *
 *-----------------------------------------------------------------*/

 class Ship
{	
	public:
		
	int size;
	vector <Coordinate> units;
	vector <Coordinate> sunkUnits;
		
	void create(int size, struct Coordinate units[5])
	{
		int i;
		
		this->size = size;
		
		for(i = 0 ; i < this->size ; i++){
				this->units.push_back(units[i]);
		}

		
	}
	

	bool checkHit(struct Coordinate *unit)
	{
		int i;
		
		bool hit = false;
		
		for(i = 0 ; i < this->units.size() ; i++){
			if(this->units[i].row == unit->row && this->units[i].col == unit->col){
				
				if(!this->isUnitSunk(unit)){
					this->sunkUnits.push_back(this->units[i]);
					hit = true;	
				}
	
			}
		}
		
		return hit;

	}
	
	bool isUnitSunk(struct Coordinate *unit)
	{	
		int i;	
		bool sunk = false;
		
		for(i = 0 ; i < this->sunkUnits.size() ; i++){
			if(this->sunkUnits[i].row == unit->row && this->sunkUnits[i].col == unit->col){

				sunk = true;	
			}
		}
		
		return sunk;
	}
	
	bool isSunk()
	{	
		
		bool sunk = false;
		
		if(this->sunkUnits.size() == this->size){
			sunk = true;
		}
		
		return sunk;
	}
	
	static bool hasValidPosition(struct Coordinate *unit){
		
		bool validPosition = false;
		
		if(
			unit->row >= 1
			&& unit->row <= 10
			&& unit->col >= 1
			&& unit->col <= 10
		){
			validPosition = true;	
		}
			
		return validPosition;
	}
	
};


/*-----------------------------------------------------------------*
 * Board Class										               *
 *-----------------------------------------------------------------*/

class Board
{	

	public:
	vector <Ship> ships;

	private:
	vector <Coordinate> shipsUnits;
	
	int shipsScheme[5];
	
	Coordinate currentUnit;
	vector <Coordinate> borders;
	vector <Coordinate> missedHits;

	void setScheme(){
		int i;
		
		// Liczba statk?w: 1 jednomasztowie, 2 dwumasztowce, 4 trzymasztowce, 2 czretomasztowce i 1 pi?ciomasztowiec
		this->shipsScheme[0] = 1;
		this->shipsScheme[1] = 2;
		this->shipsScheme[2] = 4;
		this->shipsScheme[3] = 2;
		this->shipsScheme[4] = 1;
		
	}
		
	/**
	 * Sprawdza, czy po?o?enie jednostek jest takie same
	 */
	bool unitsEqual(struct Coordinate *unit1, struct Coordinate *unit2){
		
		bool equal = false;

		if(unit1->row == unit2->row && unit1->col == unit2->col){
			equal = true;
		}
		
		return equal;
		
	}	
		
	/**
	 * Sprawdza, czy dany punkt jest wolny
	 */
	bool isLocationFree(struct Coordinate *unitToCheck){
		int i;
		
		bool free = true;
		
			for(i=0 ; i < shipsUnits.size() ; i++){

					if(this->unitsEqual(unitToCheck, &shipsUnits[i])){
						free = false;
					}		
							
			}
		
		return free;
		
	}
	
	
	/**
	 * Sprawdza, czy dany punkt nie ma swoim otoczeniu jednostek p?ywaj?cych
	 */
	bool hasFreeBorders(struct Coordinate *unitToCheck){
		int i;
	
		bool free = true;
		
		this->flushBorders();
		this->setBorders(unitToCheck);
		
	
		for(i=0 ; i < this->borders.size() ; i++){
				
				if(!this->isLocationFree(&this->borders[i])){
					free = false;
					break;
				}	
		}
		
		
		// Debug
		if(debugMode){
			cout << "hasFreeBorders" << endl;
			cout << "Punkt " << unitToCheck->row << " " << unitToCheck->col << endl;
			cout << "Granice: " << endl;
			for(i=0 ; i < this->borders.size() ; i++){
				cout << this->borders[i].row << " " << this->borders[i].col << " | ";
			}
			cout << endl;
			if(free){
				cout << "Ma wolne granice";
			}else{
				cout << "Nie ma wolnych granic";
			}
			cout << endl <<endl;
			
		}
		
		
		return free;
		
	}

	
	void setBorders(struct Coordinate *unit){
		int i;
		int row = unit->row;
		int col = unit->col;
		Coordinate tmpPoint;
		
		Coordinate coordinates[8];
		
	 	Coordinate unit1{row -1, col-1}; // G?ra lewa
		coordinates[0] = unit1;
		
		Coordinate unit2{row -1, col}; // G?ra 
		coordinates[1] = unit2;
		
		Coordinate unit3{row - 1, col+1}; // G?ra Prawa
		coordinates[2] = unit3;
		
		Coordinate unit4{row , col-1}; // Lewa
		coordinates[3] = unit4;
		
		Coordinate unit5{row, col+1}; // Prawa
		coordinates[4] = unit5;
		
		Coordinate unit6{row+1, col-1}; // D? lewy
		coordinates[5] = unit6;
		
		Coordinate unit7{row+1, col}; // D?
		coordinates[6] = unit7;
		
		Coordinate unit8{row+1, col+1}; // D? prawy
		coordinates[7] = unit8;
		

		// Zapisz tylko te w obrebie planszy
		for(i=0 ; i < 8 ; i++){
			if(Ship::hasValidPosition(&coordinates[i])){
				this->borders.push_back(coordinates[i]);
			}
		}
		
	}
	
	
	void flushBorders(){
		vector<Coordinate>().swap(this->borders);
	}

	bool canDeployUnit(struct Coordinate *location){
		bool canDeploy = false;
		
		if(this->isLocationFree(location) && this->hasFreeBorders(location)){
			canDeploy = true;
		}
		
		return canDeploy;
	}
	
	/**
	 * Losouje polozenie wszytskich statkow dla jednego gracza
	 */
	void deployShips()
	{
    	int i;
    	
		
		for(i=4 ; i >= 0 ; i--){
				int j;
				int shipSize = i + 1; // Rozmiar statku to indeks tablicy + 1
				int totalShipsInSize = this->shipsScheme[i]; // Liczba statk?w w tym rozmiarze
				
	
				for(j=0 ; j < totalShipsInSize ; j++){
					
					bool shipDeployed = false;
	
					while( !shipDeployed ){
					
				
		
						bool goodLocationToDeploy = false;
						Coordinate startLocation;
						
						/**
						 * Szuka potencjalnie dobrej lokalizacji do zwodowania statku
						 * Zak?ada si?, ze pozycja oraz jej otoczenie musi by? wolne od innych statk?w
						 */ 
						while( !goodLocationToDeploy ){	
				    		
				    		startLocation.row = rand() % 10 + 1;  // Los od 1 do 10
				    		startLocation.col = rand() % 10 + 1;  // Los od 1 do 10
				    		
				    		if(this->canDeployUnit(&startLocation)){
				    			goodLocationToDeploy = true;	
					    	}
				        
				    	};
				    	
				    	
				    	if(debugMode){
							cout << endl << endl << "Znaleziono dobra lokaliacje startowa: " << startLocation.row << " " << startLocation.col << endl << endl; 
						}
						
						
						/**
						  * Pr?ba wodowania statku
						  * Statek jest budowany w losowym kierunku. Je?eli w ?adnym kierunku nie uda si? go zbudowa?
						  * to losowany jest jest nowy punkt startowy i powtarzana jest pr?ba zbudowania statku.
						  */
						Coordinate shipBody[shipSize];
						int directions[4] = {1, 2, 3, 4}; // Kierunki g?ra, prawo, do?, lewo
						random_shuffle(&directions[0], &directions[3]);
						
						int k, l, m;
						shipBody[0] = startLocation;
		
					    for(k=0 ; k < 4 ; k++){	
					    	
					    	bool canCreateThisWay = true;
					    
				    		for(l=1 ; l < shipSize ; l++){
				    			
				    			switch(directions[k]){
				    				
				    				// Szukaj ?cie?ki w g?r?
								    case 1:
								    	shipBody[l].row = shipBody[l-1].row - 1;
								    	shipBody[l].col = shipBody[l-1].col;
							
								    	if(!this->canDeployUnit(&shipBody[l])){
								    		canCreateThisWay = false;
										}
										
								    	break;
								    	
								    // Szukaj ?cie?ki w prawo	
								    case 2:
								        shipBody[l].row = shipBody[l-1].row;
								    	shipBody[l].col = shipBody[l-1].col + 1;
							
								    	if(!this->canDeployUnit(&shipBody[l])){
								    		canCreateThisWay = false;
										}
								       	
								    	break;
								    	
								    // Szukaj ?cie?ki w d?
								    case 3:
								        shipBody[l].row = shipBody[l-1].row + 1;
								    	shipBody[l].col = shipBody[l-1].col;
							
								    	if(!this->canDeployUnit(&shipBody[l])){
								    		canCreateThisWay = false;
										}
								        
								    	break;
								    	
								    // Szukaj ?cie?ki w lewo		
							        case 4:
							        	shipBody[l].row = shipBody[l-1].row;
								    	shipBody[l].col = shipBody[l-1].col -1;
							
								    	if(!this->canDeployUnit(&shipBody[l])){
								    		canCreateThisWay = false;
										}
							        	
							    		break;	
								}
								
								if(!canCreateThisWay){
									
									for(m=1 ; m < shipSize ; m++){
										shipBody[m].row = 0;
										shipBody[m].col = 0;
									}
									
									break;
								}
				    			
					    		
				       		}
				       		
				       		
				       		shipDeployed = true;
				       		for(l=0 ; l < shipSize ; l++){
				       			if(!Ship::hasValidPosition(&shipBody[l])){
								 	shipDeployed = true;
								}
							}
				       		
				       		if(shipDeployed){
				       			break;
							}
				       		
				       		
				    	};
			    	
			    			
						// Czy uda?o si? zwodowa? statek?
					
						if(shipDeployed){
							
							Ship ship;
							ship.create(shipSize, shipBody);
							this->ships.push_back(ship);
							
							for(k=0 ; k < shipSize ; k++){
								this->shipsUnits.push_back(shipBody[k]);
							}
							
						}
			    			
			    	}
					
				}
					
		}
	}
	
	public:
	create(){
		this->setScheme();
		this->deployShips();
	}

	void logMissedHit(struct Coordinate *&unit)
	{
		this->missedHits.push_back(*unit);

	}
	
	bool isMissedHit(int row, int col)
	{
		bool isMissed = false;
		int i;
		
		Coordinate currentUnit;
		currentUnit.row = row;
		currentUnit.col = col;
					

		for(i = 0 ; i < this->missedHits.size() ; i++){
			if(this->missedHits[i].row == row && this->missedHits[i].col == col){
				isMissed = true;
				break;
			}
		}
		
		return isMissed;
	}
	
};

/*-----------------------------------------------------------------*
 * Abstract Player Class										   *
 *-----------------------------------------------------------------*/

class Player
{
	protected:
	string name;
	Board board;

	public:
	Player(string name){
		
		this->board.create();
	
		this->name = name;
	
	}
	
	int hitType(struct Coordinate *hitPosition){
		int i;
		int type = 1;
		
		for(i = 0 ; i < this->board.ships.size() ; i++){
			if(this->board.ships[i].checkHit(hitPosition)){
				
				if(this->board.ships[i].isSunk()){
					type = 3;
				}else{
					type = 2;
				}
				
			}
		}
		
		if(type == 1){
			this->board.logMissedHit(hitPosition);
		}
		

		//@TODO Z jakiegos powodu funkcja isSunk() nie zwraca typu "zatopiony" dla jednomasztowca. Sprawdzic!

		return type;
		
	}
	
	
	debugPlayer(){
		int i,j;
		
		cout << "********* Gracz "<< this->name << " ************" << endl << endl;
		
		for(i = 0 ; i < this->board.ships.size() ; i++){
			
			switch(this->board.ships[i].size){
				case 1:
					cout << "Jednomasztowiec ";
				break;
				case 2:
					cout << "Dwumasztowiec ";
				break;
				case 3:
					cout << "Trzymasztowiec ";
				break;
				case 4:
					cout << "Czteromasztowiec ";
				break;
				case 5:
					cout << "Pieciomasztowiec ";
				break;
			}
			
			
			for(j = 0 ; j < this->board.ships[i].units.size() ; j++){
				
				cout << this->board.ships[i].units[j].row << " ";
				cout << this->board.ships[i].units[j].col << " | ";
				
			}
			
			cout << endl << endl;
			
		}
			
	}
		
};
 
 
/*-----------------------------------------------------------------*
 * Player Human Class										       *
 *-----------------------------------------------------------------*/
  
class PlayerHuman : public Player
{
	public:
	PlayerHuman(string name) : Player(name)
    {
       
    }
	
	printBoard()
	{
		int i,j,k,m;
		
		string margin = "     ";
		
		cout << endl << endl;
		cout << margin << "+----------------------+" << endl;
		cout << margin << "|     Twoja plansza    |" << endl;
		cout << margin << "+----------------------+" << endl;

		cout << margin << " #|A|B|C|D|E|F|G|H|I|J|" << endl;
		for(k = 1 ; k <= 10 ; k++){
			
			if(k!= 10){
				cout << margin << " " << k << "|";
			}else{
				cout << margin << k << "|";
			}
			
			
			for(m = 1 ; m <= 10 ; m++){
				
				bool isShip = false;
				bool isShipSunk = false;
				bool isUnitSunk = false;
				
				for(i = 0 ; i < this->board.ships.size() ; i++){
			
					for(j = 0 ; j < this->board.ships[i].units.size() ; j++){
						
						if(this->board.ships[i].units[j].row == k && this->board.ships[i].units[j].col == m){
							isShip = true;
							isUnitSunk = this->board.ships[i].isUnitSunk(&this->board.ships[i].units[j]);
						break;
						}
					}
					
					isShipSunk = this->board.ships[i].isSunk();
					
					if(isShip){
						break;
					}
			
				}
				
				if(isShip){
					if(isShipSunk){
						 cout << startRed << (char)219 << endColor << " ";
						 //cout << (char)251 << " ";
					}else if(isUnitSunk){
						cout << startYellow << (char)219 << endColor << " ";
						//cout << (char)234 << " ";
					}else{
						cout << (char)219 << " ";
					}
				}else{
					if(this->board.isMissedHit(k,m)){
						cout << (char)158 << " ";
					}else{
						cout << (char)176 << " ";
					}
					
				}
					
			}
		
			cout << endl;
		}
		
	} 
	
    
};  
  
  
/*-----------------------------------------------------------------*
 * Player Computer Class										   *
 *-----------------------------------------------------------------*/
 
 class PlayerCom : public Player
{
	public:
	PlayerCom(string name) : Player(name)
    {
       
    }
       
	printBoard()
	{
		int i,j,k,m;
		
		string margin = "     ";
		
		cout << endl << endl;
		cout << margin << "+----------------------+" << endl;
		cout << margin << "|  Plansza przeciwnika |" << endl;
		cout << margin << "+----------------------+" << endl;
		
		cout << margin << " #|A|B|C|D|E|F|G|H|I|J|" << endl;
		for(k = 1 ; k <= 10 ; k++){
			
			if(k!= 10){
				cout << margin << " " << k << "|";
			}else{
				cout << margin << k << "|";
			}
			
			
			for(m = 1 ; m <= 10 ; m++){
				
				bool isShip = false;
				bool isShipSunk = false;
				bool isUnitSunk = false;
				
				for(i = 0 ; i < this->board.ships.size() ; i++){
			
					for(j = 0 ; j < this->board.ships[i].units.size() ; j++){
						
						if(this->board.ships[i].units[j].row == k && this->board.ships[i].units[j].col == m){
							isShip = true;
							isUnitSunk = this->board.ships[i].isUnitSunk(&this->board.ships[i].units[j]);
							break;
						}
						
					}
					
					isShipSunk = this->board.ships[i].isSunk();
					
					if(isShip){
						break;
					}
			
				}
				

				if(isShip){
					if(isShipSunk){
						 cout << startRed << (char)219 << endColor << " ";
						// cout << (char)251 << " ";
					}else if(isUnitSunk){
						cout << startYellow << (char)219 << endColor << " ";
						// cout << (char)234 << " ";
					}else{
						if(showComMap){
								cout << (char)219 << " ";
						}else{
							cout << (char)176 << " ";
						}
					
					}
					
				}else{
					if(this->board.isMissedHit(k,m)){
						cout << (char)158 << " ";
					}else{
						cout << (char)176 << " ";
					}
				}
					
			}
		
			cout << endl;
		}
		
	}  
    
    Coordinate hit(){
    	
    	Coordinate point;
    	point.row = rand() % 10 + 1;  // Los od 1 do 10
		point.col = rand() % 10 + 1;  // Los od 1 do 10
    	
    	return point;
	}
	
	string getHitString(struct Coordinate *hit){
		
		char chars[3];
		
		int x = hit->row;
		int y = hit->col;
		
		chars[0] = y + 64;
		chars[1] = x + 48;
		chars[2] = NULL;
		
		if(x == 10){
			chars[1] = '1';
			chars[2] = '0';
		}
		
		return string(chars);

	}
    
};  

/*-----------------------------------------------------------------*
 * Helpersy								                		   *
 *-----------------------------------------------------------------*/

bool hitTranslate(string *hit, struct Coordinate *point){
	
	if(hit->length() == 2 || hit->length() == 3){
		
	char chars[3];
	strcpy(chars, hit->c_str());
		

	if(chars[0] >= 'A' && chars[0] <= 'J'){
	  	
	  	if(chars[1] >= '0' && chars[1] <= '9' && chars[2] != '0'){
		  	
		  	point->col = chars[0] - 64;	
		  	point->row = chars[1] - 48; 
		  	return true;
		}
	  		
	  	
		  	
		if(chars[1] == '1' && chars[2] == '0'){
		  	
		  	point->col = chars[0] - 64;	
		  	point->row = 10; 
		  	return true;
		}
	  	 	
		  		
	}
		  	
		  
	}
	
	return false;
}

void setWindowSize(){
	HWND console = GetConsoleWindow();
    RECT r;
    GetWindowRect(console, &r); //stores the console's current dimensions

    MoveWindow(console, r.left, r.top, 325, 700, TRUE); // 800 width, 100 height
}

void preloader(){
	int i;
	
	for(i = 0 ; i < 20 ; i++){
		Sleep(100);
		cout << ".";
	}
}

/*-----------------------------------------------------------------*
 * Main										                       *
 *-----------------------------------------------------------------*/
int main(int argc, char** argv) {
	
	setWindowSize();
    
	srand(time(NULL));
	
	bool running = true;
	system("cls");
	
	PlayerCom pCom("Przeciwnik");
	pCom.printBoard();
	
	PlayerHuman pHuman("Damian");
	pHuman.printBoard();
	
	
	while(running){
			
		string hit;
	
		bool incorrectHitFormat = true;
		int hitType;
		int comHitType;
		
		Coordinate humanHitCoordinate;
		Coordinate comHitCoordinate;
		
		cout << endl << "Ostrzelaj pole: ";
		
		do { 
	        cin >> hit;
		        
		    
		    if(!hitTranslate(&hit, &humanHitCoordinate)){
				cout << endl << "NieprawidĚ�??owy format koordynat�?�?w. Poprawna forma to np. \"B10\"" << endl;
				cout << endl << "Ostrzelaj pole: ";
			}else{
			  incorrectHitFormat = false;
			}    
		        
	    } while (incorrectHitFormat);
		
		system("cls");
		
		hitType= pCom.hitType(&humanHitCoordinate);
		
		pCom.printBoard();
		pHuman.printBoard();
		incorrectHitFormat = true;
		
		cout << endl;
		
		switch(hitType){
				case 1:
					cout << "Pudlo!!!";
				break;
				case 2:
					cout << "Trafiony. Plynie dalej.";
				break;
				case 3:
					cout << "Trafiony zatopiony!!!";
				break;
			}
		 
		 string compHit;
		 cout << endl << "Ruch przeciwnika..." << endl;
		 
		 preloader();
		 
		 comHitCoordinate = pCom.hit();
		  
		 
		 comHitType = pHuman.hitType(&comHitCoordinate);
		 
		 system("cls");
		 
		 pCom.printBoard();
		pHuman.printBoard();
		 
		 cout << endl;
		 switch(comHitType){
				case 1:
					cout << "Komputer:" << pCom.getHitString(&comHitCoordinate) << " Pudlo!!!";
				break;
				case 2:
					cout << "Komputer:" << pCom.getHitString(&comHitCoordinate) << " Trafiony. Plynie dalej.";
				break;
				case 3:
					cout << "Komputer:" << pCom.getHitString(&comHitCoordinate) << " Trafiony zatopiony!!!";
				break;
			}
		
	}
		
	
}
